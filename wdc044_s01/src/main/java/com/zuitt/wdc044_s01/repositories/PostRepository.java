package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.modals.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {

    // test
//    Post findByAuthor(String username);
}
