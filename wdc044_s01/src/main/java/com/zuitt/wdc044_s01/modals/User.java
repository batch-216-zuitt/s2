package com.zuitt.wdc044_s01.modals;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    // Table Columns
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore // to avoid infinite recursion
    private Set<Post> posts;

    public Set<Post> getPosts() {
        return posts;
    }



    // Empty Constructor
    public User(){}

    // Parameterized Constructor
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }


    // Getters & Setters
    public Long getId() { return id; }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
