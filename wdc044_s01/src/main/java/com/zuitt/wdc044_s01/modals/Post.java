package com.zuitt.wdc044_s01.modals;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {

    // Table Columns
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;


    // Empty Constructor
    public Post(){};

    // Parameterized Constructor
    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }


    // Getters & Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() { return user; }
    public void setUser(User user) {  this.user = user;  }
}
