package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.config.JwtToken;
import com.zuitt.wdc044_s01.modals.Post;
import com.zuitt.wdc044_s01.modals.User;
import com.zuitt.wdc044_s01.repositories.UserRepository;
import com.zuitt.wdc044_s01.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();

        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        }

        return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
    }

    // Mini Activity
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        }

        return new ResponseEntity<>("You are not authorized to delete this post", HttpStatus.UNAUTHORIZED);
    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    public Iterable<Post> getMyPosts(String stringToken) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        return author.getPosts();
    }

    // test
//    public Iterable<Post> getPostsOfUser(String stringToken) {
//        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
//        return postRepository.findPost(user);
//    }
}
